<?php

require_once dirname(__FILE__).'/../lib/photosGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/photosGeneratorHelper.class.php';

/**
 * photos actions.
 *
 * @package    Galleryne
 * @subpackage photos
 * @author     leny
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class photosActions extends autoPhotosActions
{
}
