<?php slot('page_title'); ?>
<a href="<?php echo url_for("gallery/new") ?>" class="btn right">Ajouter une galerie</a>
    <h2><?php echo __("backend.gallery.index.title",array(),"galleryne"); ?></h2>
<?php end_slot(); ?>

<?php include_partial('gallery/assets') ?>
<?php if($pager->count()){ ?>

<div style="margin:10px;" class="panther-container">
        <?php foreach ($pager->getResults() as $nb=>$g) { ?>
        <div style="background-image: url('<?php echo $g->getPhotoDefault(null) ?>');background-position: center;overflow: hidden;">
            <a href="<?php echo url_for("gallery/edit?id=".$g->getId()) ?>" title="<?php echo __("backend.gallery.index.edit",array(),"galleryne"); ?>">
                    <?php echo $g->getTitle() ?> - Modifier </a>
            <div class="clear"></div>
            <?php $panther = GalleryneUtils::preparePanther($g->getPhotos()); ?>
            <div style="width: 50%;float: left">
                <ul id="gallery-<?php echo $g->getId();?>-left" style="width: 300px;float: right">
                    <?php include_partial('gallery/panther', array('photos'=>$panther['left'],'float'=>'right','heights'=>$panther['heights'])); ?>
                </ul>
            </div>
            <div style="width: 50%;float: right">
                <ul id="gallery-<?php echo $g->getId();?>-right" style="width: 300px;">
                    <?php include_partial('gallery/panther', array('photos'=>$panther['right'],'float'=>'left','heights'=>$panther['heights'])); ?>
                </ul>
            </div>
        </div>
        <?php } ?>
        

    <div style="float: right">
        <?php if ($pager->haveToPaginate()){ ?>
            <ul class="pagination">
                <?php foreach ($pager->getLinks() as $page){ ?>
                  <li <?php echo $page == $pager->getPage() ? "class='current'": ""; ?>>
                      <!-- this way we remember where the user is (gallery and page -->
                      <a href="<?php echo preg_replace("/\?[a-z\-&=0-9]+/","",$_SERVER["REQUEST_URI"]) ?><?php echo isset($_GET["gallery"])? "?gallery=".$_GET["gallery"]."&":"?"; ?>page=<?php echo $page ?>">
                          <span><?php echo $page ?></span>
                      </a>
                  </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>
</div>
<?php }else{ ?>
<?php echo __("backend.gallery.index.empty",array(),"galleryne"); ?>
<?php } ?>
<style>
    .panther-container ul{
        width: 600px;
        margin: 0;
        padding: 0;
    }
    .panther-container ul,
    .panther-container ul li{
        list-style: none;
        padding: 0;
    }
 </style>